﻿using ExCoreEngine.Clauses;
using System;

namespace AiExp
{

    class Program
    {
        static void Main(string[] args)
        {
            using (var engineController = new EngineControl())
            {
                engineController.Init();
            }
        }
    }
}
