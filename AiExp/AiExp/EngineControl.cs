﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExCoreEngine.BaseModules;
using System.Text.Json;
using ExCoreEngine.Clauses;
using Newtonsoft.Json;

namespace AiExp
{
    public class EngineControl : IDisposable
    {
        public string BaseDataDirectory => Directory.GetCurrentDirectory() + @"\ExperienceModel";
        private string errorNotification = "Неправильно введена команда или произошла ошибка, попытайтесь снова.";
        private LogicIoMachine ExCoreEngine;
        private FileStream ruleSet;
        public EngineControl()
        {
            ExCoreEngine = new LogicIoMachine();
            if (!Directory.Exists(BaseDataDirectory))
            {
                Directory.CreateDirectory(BaseDataDirectory);
            }
            ruleSet = new FileStream(BaseDataDirectory + @"\Rules.json", FileMode.OpenOrCreate, FileAccess.ReadWrite);
        }
        public void Init()
        {
            int launchCase = 0;
            IntegrityModule integrityModule = null;
            if (ruleSet.Length != 0)
            {
                try
                {
                    var ruleSetReader = new StreamReader(ruleSet, Encoding.UTF8);
                    integrityModule = new IntegrityModule(JsonConvert.DeserializeObject<List<Rule>>(ruleSetReader.ReadToEnd()));
                    ExCoreEngine.RuleSet = integrityModule.EnshuredRules;
                    ruleSetReader.Close();
                }
                catch (Exception E)
                {
                    Console.WriteLine(E.Message);
                    Console.ReadLine();
                }
            }
            else
            {
                ruleSet.Close();
            }
            while (launchCase != 3)
            {
                Console.Clear();
                Console.WriteLine(@"Выберите действие:
1. Добавить правило.
2. Найти решение.
3. Выход.");
                try
                {
                    Console.Write("Выберите функцию: ");
                    launchCase = int.Parse(Console.ReadLine());
                    switch (launchCase)
                    {
                        case 1:
                            Console.Clear();
                            ExCoreEngine.RuleSet.Add(CreateRule());
                            break;
                        case 2:
                            Console.Clear();
                            Solve();
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine(errorNotification);
                }
            }
        }
        private Rule CreateRule()
        {
            int launchCase = 0;
            Console.Write("Ввведите имя правила: ");
            var rule = new Rule(Console.ReadLine());
            while (launchCase != 1)
            {
                try
                {
                    rule.AddAntecedent(CreateClause());
                    Console.WriteLine("Введите 1 для завершения создания условий или нажмите ввод.");
                    string input = Console.ReadLine();
                    launchCase = input == "" ? 0 : int.Parse(input);
                }
                catch
                {
                    Console.WriteLine(errorNotification);
                }
            }
            Console.WriteLine("Введите параметры заключения");
            var clause = new IsClause();
            Console.Write("Введите название параметра: ");
            clause.Variable = Console.ReadLine();
            Console.Write("Введите величину: ");
            clause.Value = Console.ReadLine();
            rule.AddConsequent(clause);
            return rule;
        }

        private Clause CreateClause()
        {
            Clause clause = null;
            Console.WriteLine(@"Выберите тип удтверждения:
1. Постоянное удтверждение.
2. Удтверждение меньшинства.
3. Удтверждение меньшинства или равенства.
4. Удтвержение большинства.
5. Удтвержение большинства или равенства.");
            try
            {
                Console.Write("Введите выбранный вариант: ");
                switch (int.Parse(Console.ReadLine()))
                {
                    case 1:
                        clause = new IsClause();
                        Console.Write("Введите название параметра: ");
                        clause.Variable = Console.ReadLine();
                        Console.Write("Введите величину: ");
                        clause.Value = Console.ReadLine();
                        break;
                    case 2:
                        clause = new LessClause();
                        Console.Write("Введите название параметра: ");
                        clause.Variable = Console.ReadLine();
                        Console.Write("Введите величину: ");
                        clause.Value = Console.ReadLine();
                        break;
                    case 3:
                        clause = new LessEqualClause();
                        Console.Write("Введите название параметра: ");
                        clause.Variable = Console.ReadLine();
                        Console.Write("Введите величину: ");
                        clause.Value = Console.ReadLine();
                        break;
                    case 4:
                        clause = new GreaterClause();
                        Console.Write("Введите название параметра: ");
                        clause.Variable = Console.ReadLine();
                        Console.Write("Введите величину: ");
                        clause.Value = Console.ReadLine();
                        break;
                    case 5:
                        clause = new GreaterEqualClause();
                        Console.Write("Введите название параметра: ");
                        clause.Variable = Console.ReadLine();
                        Console.Write("Введите величину: ");
                        clause.Value = Console.ReadLine();
                        break;
                    default:
                        break;
                }
            }
            catch
            {
                Console.WriteLine(errorNotification);
            }
            return clause;
        }
        private void ChooseFactsFromSystem()
        {
            int launchCase = 0;
            var clauses = new List<Clause>();
            Clause selectedClause = null;
            foreach (var rule in ExCoreEngine.RuleSet)
            {
                clauses.AddRange(rule.Antecendents);
            }
            var clausesQuery = clauses.Take(10).Distinct().ToArray();
            while (launchCase != 1 && Array.IndexOf(clauses.ToArray(), selectedClause) != clauses.Count - 1)
            {
                try
                {
                    for (int i = 0; i != clausesQuery.Length; i++)
                    {
                        Console.WriteLine($"{i + 1}. {clausesQuery[i]}");
                    }
                    Console.WriteLine("Введите номер факта из списка");
                    selectedClause = clausesQuery[int.Parse(Console.ReadLine()) - 1];
                    ExCoreEngine.WorkingMemory.Facts.Add(selectedClause);
                    Console.WriteLine("Введите 1 для завершения выбора фактов или нажмите ввод чтобы продолжить.");
                    clausesQuery = clauses.Skip(Array.IndexOf(clauses.ToArray(), selectedClause) + 1).Distinct().Take(10).ToArray();
                    string input = Console.ReadLine();
                    launchCase = input == "" ? 0 : int.Parse(input);
                }
                catch
                {
                    Console.WriteLine(errorNotification);
                }
            }
        }
        private void AddFactsManually()
        {
            int launchCase = 0;
            while (launchCase != 1)
            {
                try
                {
                    ExCoreEngine.WorkingMemory.Facts.Add(CreateClause());
                    Console.WriteLine("Введите 1 для завершения введения фактов или нажмите ввод чтобы продолжить.");
                    string input = Console.ReadLine();
                    launchCase = input == "" ? 0 : int.Parse(input);
                }
                catch
                {
                    Console.WriteLine(errorNotification);
                }
            }
        }
        private void Solve()
        {
            int launchCase = 0;
            while (launchCase != 3)
            {
                try
                {
                    Console.WriteLine(@"Выберите источник фактов:
1.Факты из набора правил и базы знаний.
2.Ручной ввод.
3.Перейти к решению");
                    launchCase = int.Parse(Console.ReadLine());
                    switch (launchCase)
                    {
                        case 1:
                            Console.Clear();
                            ChooseFactsFromSystem();
                            break;
                        case 2:
                            Console.Clear();
                            AddFactsManually();
                            break;
                    }
                }
                catch (Exception E)
                {
                    Console.WriteLine(errorNotification);
                    Console.WriteLine(E.Message);
                    Console.ReadLine();
                }
            }
            /*Console.WriteLine("Содержимое рабочей памяти");
            Console.WriteLine(ExCoreEngine.WorkingMemory.ToString());
            Console.ReadKey();
            ExCoreEngine.Infer();
            Console.WriteLine("Заключение по методу прямого вывода");
            Console.WriteLine(ExCoreEngine.WorkingMemory.ToString());
            Console.ReadKey();*/ 
            Console.WriteLine("Уточните заключающий факт");
            Console.WriteLine($"Заключение системы {ExCoreEngine.Infer(Console.ReadLine(), new List<Clause>())}");
            Console.ReadKey();
            ExCoreEngine.WorkingMemory.Facts.Clear();
        }

        public void Dispose()
        {
            try
            {
                using (var ruleSetWriter = new StreamWriter(ruleSet.Name, false, Encoding.UTF8))
                {
                    ruleSetWriter.WriteLine(JsonConvert.SerializeObject(ExCoreEngine.RuleSet));
                    ruleSetWriter.Close();
                }
            }
            catch (Exception E)
            {
                Console.WriteLine(E.Message);
            }
            finally
            {
                ruleSet.Dispose();
            }
        }
    }

}
