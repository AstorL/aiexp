﻿using ExCoreEngine.Clauses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace ExCoreEngine.BaseModules
{
    [Serializable]
    public class WorkingMemory
    {
        public WorkingMemory() => Facts = new List<Clause>();
        [JsonPropertyName("facts")]
        public List<Clause> Facts { get; private set; }
        public bool IsFact(Clause clause)
        {
            foreach (var fact in Facts)
            {
                if (fact.Match(clause) == Intersection.INCLUDE)
                {
                    return true;
                }
            }
            return false;
        }
        public bool IsNotFact(Clause clause)
        {
            foreach (var fact in Facts)
            {
                if (fact.Match(clause) == Intersection.MUTUALLY_EXCLUDE)
                {
                    return true;
                }
            }
            return false;
        }
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            foreach(var fact in Facts)
            {
                stringBuilder.Append(fact.ToString()+'\n');
            }
            return stringBuilder.ToString();
        }
    }
}
