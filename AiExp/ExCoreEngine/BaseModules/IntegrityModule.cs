﻿using ExCoreEngine.Clauses;
using ExCoreEngine.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExCoreEngine.BaseModules
{
    public class IntegrityModule
    {
        public List<Rule> EnshuredRules { get; private set; }
        public IntegrityModule(List<Rule> unEnshuredRules)
        {
            EnshuredRules = new List<Rule>();
            foreach (var rule in unEnshuredRules)
            {
                var enshuredRule = new Rule(rule.Name);
                foreach (var antecendent in rule.Antecendents)
                {
                    switch (antecendent.ClauseType)
                    {
                        case ClauseTypes.ISCLAUSE:
                            var isClause = new IsClause { Variable = antecendent.Variable, Value = antecendent.Value };
                            enshuredRule.AddAntecedent(isClause);
                            break;
                        case ClauseTypes.LESSCLAUSE:
                            var lessClause = new LessClause { Variable = antecendent.Variable, Value = antecendent.Value };
                            enshuredRule.AddAntecedent(lessClause);
                            break;
                        case ClauseTypes.LESSEQUALCLAUSE:
                            var lessEqualClause = new LessEqualClause { Variable = antecendent.Variable, Value = antecendent.Value };
                            enshuredRule.AddAntecedent(lessEqualClause);
                            break;
                        case ClauseTypes.GREATERCLAUSE:
                            var greaterClause = new GreaterClause { Variable = antecendent.Variable, Value = antecendent.Value };
                            enshuredRule.AddAntecedent(greaterClause);
                            break;
                        case ClauseTypes.GREATEREQUALCLAUSE:
                            var greaterEqualClause = new GreaterEqualClause { Variable = antecendent.Variable, Value = antecendent.Value };
                            enshuredRule.AddAntecedent(greaterEqualClause);
                            break;
                    }
                }
                enshuredRule.Consequent = rule.Consequent;
                EnshuredRules.Add(enshuredRule);
            }
        }
    }
}
