﻿using ExCoreEngine.Clauses;
using ExCoreEngine.Enums;
using System.Collections.Generic;
using System.Linq;

namespace ExCoreEngine.BaseModules
{

    public class LogicIoMachine
    {
        public List<Rule> RuleSet { get; set; }
        public WorkingMemory WorkingMemory { get; set; }
        public LogicIoMachine()
        {
            RuleSet = new List<Rule>();
            WorkingMemory = new WorkingMemory();
        }
        private bool FireRules(List<Rule> conflictingRules)
        {
            bool fireableRules = false;
            foreach (var rule in conflictingRules)
            {
                if (!rule.Fired)
                {
                    fireableRules = true;
                    rule.Fire(WorkingMemory);
                }
            }
            return fireableRules;
        }
        private bool IsFact(Clause goal, List<Clause> unproved_conditions)
        {
            var goal_stack = from rule in RuleSet where rule.Consequent.Match(goal) == Intersection.INCLUDE select rule;
            
            if (goal_stack.Count() == 0)
            {
                unproved_conditions.Add(goal);
            }
            else
            {
                foreach (var rule in goal_stack)
                {
                    int i= 0;
                    bool goal_reached = true;
                    while (i!=rule.Antecendents.Count)
                    {
                        var antecedent = rule.Antecendents[i];
                        if (!WorkingMemory.IsFact(antecedent))
                        {
                            if (WorkingMemory.IsNotFact(antecedent))
                            {
                                goal_reached = false;
                                break;
                            }
                            else if (IsFact(antecedent, unproved_conditions))
                            {
                                WorkingMemory.Facts.Add(antecedent);
                            }
                            else
                            {
                                goal_reached = false;
                                break;
                            }
                        }
                        i++;
                    }

                    if (goal_reached)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        public void Infer()
        {
           var matchingRules = new List<Rule>();
            do
            {
                matchingRules.Clear();
                foreach (var rule in RuleSet)
                {
                    if (rule.IsTriggered(WorkingMemory))
                    {
                        matchingRules.Add(rule);
                    }
                }
                if (matchingRules.Count > 0)
                {
                    if (!FireRules(matchingRules))
                    {
                        break;
                    }
                }
            } while (matchingRules.Count > 0);
        }
        public Clause Infer(string goal_variable, List<Clause> unproved_conditions)
        {
            Clause conclusion = null;
            var goalStack = new List<Rule>();
            foreach(var rule in RuleSet)
            {
                if(rule.Consequent.Variable == goal_variable)
                {
                    goalStack.Add(rule);
                }
            }
            foreach (var rule in goalStack)
            {
                int i = 0;
                bool goal_reached = true;
                while (i != rule.Antecendents.Count)
                {
                    var antecedent = rule.Antecendents[i];
                    if (!WorkingMemory.IsFact(antecedent))
                    {
                        if (WorkingMemory.IsNotFact(antecedent)) //conflict with memory
                        {
                            goal_reached = false;
                            break;
                        }
                        else if (IsFact(antecedent, unproved_conditions)) //deduce to be a fact
                        {
                            WorkingMemory.Facts.Add(antecedent);
                        }
                        else //deduce to not be a fact
                        {
                            goal_reached = false;
                            break;
                        }
                    }
                    i++;
                }

                if (goal_reached)
                {
                    conclusion = rule.Consequent;
                    break;
                }
            }

            return conclusion;
        }



    }
}
