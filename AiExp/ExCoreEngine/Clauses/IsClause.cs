﻿using ExCoreEngine.Enums;
using System;
using Newtonsoft.Json;

namespace ExCoreEngine.Clauses
{
    public class IsClause : Clause
    {
        [JsonConstructor]
        public IsClause()
        {
            Condition = "=";
            ClauseType = ClauseTypes.ISCLAUSE;
        }
        public override Intersection Intersect(Clause intersectingClause)
        {
            double intersectingValue =0;
            Intersection intersection = Intersection.UNKNOWN;
            bool isNumber = double.TryParse(Value, out double sourceValue) &&
                double.TryParse(intersectingClause.Value, out intersectingValue);
            switch (intersectingClause.ClauseType)
            {
                case ClauseTypes.ISCLAUSE:
                    intersection = Value == intersectingClause.Value ? Intersection.INCLUDE : Intersection.MUTUALLY_EXCLUDE;
                    break;
                case ClauseTypes.LESSCLAUSE:
                    intersection = isNumber && sourceValue >= intersectingValue ? Intersection.MUTUALLY_EXCLUDE : Intersection.INCLUDE;
                    break;
                case ClauseTypes.LESSEQUALCLAUSE:
                    intersection = isNumber && sourceValue > intersectingValue ? Intersection.MUTUALLY_EXCLUDE : Intersection.INCLUDE;
                    break;
                case ClauseTypes.GREATERCLAUSE:
                    intersection = isNumber && sourceValue <= intersectingValue ? Intersection.MUTUALLY_EXCLUDE : Intersection.INCLUDE;
                    break;
                case ClauseTypes.GREATEREQUALCLAUSE:
                    intersection = isNumber && intersectingValue < sourceValue ? Intersection.MUTUALLY_EXCLUDE : Intersection.INCLUDE;
                    break;
            }
            return intersection;
        }
    }
}
