﻿using ExCoreEngine.Enums;
using System;
using Newtonsoft.Json;

namespace ExCoreEngine.Clauses
{
    public class Clause
    {
        [JsonConstructor]
        public Clause(){}
        [JsonProperty("variable")]
        public string Variable { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
        [JsonIgnore]
        public string Condition { get; protected set; }
        [JsonProperty("clausetype")]
        public ClauseTypes ClauseType { get; protected set; }
        public virtual Intersection Intersect(Clause intersectingClause) => throw new NotImplementedException();
        public Intersection Match(Clause matchingClause) =>
           Value != matchingClause.Value ? Intersection.UNKNOWN : Intersect(matchingClause);
        public override string ToString() => $"{Variable} {Condition} {Value}";

    }
}
