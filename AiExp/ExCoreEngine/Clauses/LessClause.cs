﻿using ExCoreEngine.Enums;
using System;
using Newtonsoft.Json;

namespace ExCoreEngine.Clauses
{
    public class LessClause : Clause
    {
        [JsonConstructor]
        public LessClause()
        {
            Condition = "<";
            ClauseType = ClauseTypes.LESSCLAUSE;
        }
        public override Intersection Intersect(Clause intersectingClause)
        {
            Intersection intersection = Intersection.UNKNOWN;
            if (double.TryParse(Value, out double sourceValue) &&
                double.TryParse(intersectingClause.Value, out double intersectingValue))
            {
                switch (intersectingClause.ClauseType)
                {
                    case ClauseTypes.LESSCLAUSE:
                        intersection = intersectingValue <= sourceValue ? Intersection.INCLUDE : Intersection.UNKNOWN;
                        break;
                    case ClauseTypes.LESSEQUALCLAUSE:
                        intersection = intersectingValue < sourceValue ? Intersection.INCLUDE : Intersection.UNKNOWN;
                        break;
                    case ClauseTypes.ISCLAUSE:
                        intersection = intersectingValue < sourceValue ? Intersection.INCLUDE : Intersection.MUTUALLY_EXCLUDE;
                        break;
                    case ClauseTypes.GREATEREQUALCLAUSE:
                        intersection = intersectingValue >= sourceValue ? Intersection.MUTUALLY_EXCLUDE : Intersection.UNKNOWN;
                        break;
                    case ClauseTypes.GREATERCLAUSE:
                        intersection = intersectingValue >= sourceValue ? Intersection.MUTUALLY_EXCLUDE : Intersection.UNKNOWN;
                        break;
                }
            }
            return intersection;
        }
    }
}
