﻿using ExCoreEngine.BaseModules;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text;

namespace ExCoreEngine.Clauses
{
    public class Rule
    {

        [JsonConstructor]
        public Rule(string name)
        {
            Name = name;
            Antecendents = new List<Clause>();
        }
        [JsonProperty("consequent")]
        public IsClause Consequent { get; set; }
        [JsonProperty("name")]
        public string Name { get; private set; }
        [JsonIgnore]
        public bool Fired { get; set; }
        [JsonProperty("antecendents")]
        public List<Clause> Antecendents { get; set; }
        public Rule AddAntecedent(Clause clause)
        {
            Antecendents.Add(clause);
            return this;
        }
        public Rule AddConsequent (IsClause clause)
        {
            Consequent = clause;
            return this;
        }
        public void Fire(WorkingMemory workingMemory)
        {   
            if(workingMemory.IsNotFact(Consequent))
            {
                workingMemory.Facts.Add(Consequent);
            }
            Fired = true;
        }
        public bool IsTriggered(WorkingMemory workingMemory)
        {
            foreach(var antrcendent in Antecendents)
            {
                if (!workingMemory.IsFact(antrcendent))
                {
                    return false;
                }
            }
            return true;
        }
        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Insert(0, Name + '\n');
            foreach(var antecendent in Antecendents)
            {
                stringBuilder.Append(antecendent.ToString() + '\n');
            }
            stringBuilder.Append(Consequent.ToString() + '\n');
            return stringBuilder.ToString();
        }
    }
}
