﻿namespace ExCoreEngine.Enums
{
    public enum ClauseTypes
    {
        ISCLAUSE,
        LESSCLAUSE,
        LESSEQUALCLAUSE,
        GREATERCLAUSE,
        GREATEREQUALCLAUSE
    }
}
