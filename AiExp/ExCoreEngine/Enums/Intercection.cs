﻿namespace ExCoreEngine
{
    public enum Intersection
    {
        INCLUDE,
        UNKNOWN,
        MUTUALLY_EXCLUDE
    }
}